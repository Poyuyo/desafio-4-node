import { Document } from 'mongoose'

export default interface IProduct extends Document {
  _id: string;
  nombre: string;
  precio: number;
  stock: number;
  estado: string;
  ventas: Array<String>
  deleted: Boolean;
  createdAt: Date;
  updatedAt: Date;
};