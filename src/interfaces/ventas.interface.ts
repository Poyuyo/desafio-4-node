import { ObjectId } from 'mongodb';
import { Document } from 'mongoose'

export default interface ISale extends Document {
  _id: string;
  formaDePago: string;
  precioTotal: number;
  estado: string;
  persona_id: ObjectId;
  //probar ObjectId
  productos: Array<String>
  deleted: Boolean;
  createdAt: Date;
  updatedAt: Date;
};