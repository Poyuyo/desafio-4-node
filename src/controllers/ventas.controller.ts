import { Request, Response } from "express";
import Venta from "../models/ventas.model";
import IVenta from "../interfaces/ventas.interface";
import Producto from "../models/productos.model";
import IProducto from "../interfaces/productos.interface";


// Get all resources
export const index = async (req: Request, res: Response) => {
    // agregar filtros

    try {
        const { ...data } = req.query;
        let filters = { ...data };

        if (data.nombre) {
            filters = { ...filters }
        }

        let venta = await Venta.find(filters).select('-deleted').where({ deleted: false });

        res.json(venta);
    } catch (error) {
        res.status(500).send('Algo salió mal');
    }
};

// Get one resource
export const show = async (req: Request, res: Response) => {
    const id = req?.params?.id;
    try {
        let venta = await Venta.findById(id).select('-deleted').where({ deleted: false })
        .populate([{ path: 'productos', select: ['id', 'nombre', 'precio', 'stock'] }, { path: 'persona_id', select: ['id', 'nombreCompleto', 'email'] }]);;

        if (!venta)
            res.status(404).send(`No se encontró venta con id: ${id}`);
        else
            res.json(venta);
    } catch (error) {
        res.status(500).send('Algo salió mal.');
    }
};

// Create a new resource
export const store = async (req: Request, res: Response) => {
    try {
        const { ...data } = req.body;

        const venta: IVenta = new Venta({
            formaDePago: data.formaDePago,
            precioTotal: 0,
            estado: data.estado,
            persona_id: data.persona_id,
        });

        await venta.save();

        res.status(200).json(venta);
    } catch (error) {
        console.log(error);
        res.status(500).send('Algo salió mal.');
    }
};

// Edit a resource
export const update = async (req: Request, res: Response) => {
    const id = req?.params?.id;
    const { ...data } = req.body;
    try {
        let venta = await Venta.findById(id).select('-deleted').where({ deleted: false });

        if (!venta)
            return res.status(404).send(`No se encontró venta con id: ${id}`);

        if (data.formaDePago) venta.formaDePago = data.formaDePago;
        if (data.estado) venta.estado = data.estado;

        if (data.estado == "anulada") {

            let productos: IProducto[] = await Producto.find({ _id: { $in: venta.productos } })
            productos.forEach(prod => {
                prod.stock = prod.stock + 1;
                prod.save()
            })

        }

        await venta.save();

        res.status(200).json(venta);
    } catch (error) {
        console.log(error)
        res.status(500).send('Algo salió mal.');
    }
};

// Delete a resource
export const destroy = async (req: Request, res: Response) => {
    const id = req?.params?.id;
    try {
        let venta = await Venta.findById(id).select('-deleted').where({ deleted: false });

        if (!venta)
            res.status(404).send(`No se encontró el película con id: ${id}`);
        else {
            venta.deleted = true;
            venta.save();
            res.status(200).json(venta);
        }
    } catch (error) {
        res.status(500).send('Algo salió mal.');
    }
};


export const agregarProducto = async (req: Request, res: Response) => {
    const ventaId = req?.params?.ventaId;
    const productoId = req?.params?.productoId;
    try {
        let venta = await Venta.findById(ventaId).where({ deleted: false });
        let producto = await Producto.findById(productoId).where({ deleted: false });


        if (!venta || !producto) {
            res.status(404).send(`No se encontró la venta o el producto indicado.`);
        } else
            if (venta.estado == "anulada") {
                res.status(400).send(`La venta se encuentra anulada.`);
            } else
                if (producto?.stock == 0) {
                    res.status(400).send(`El producto no tiene stock.`);
                } else {
                    let indiceVenta = Number(producto?.ventas.indexOf(ventaId));
                    let indiceProducto = Number(venta?.productos.indexOf(productoId));

                    if (indiceProducto !== -1 && indiceVenta !== -1) {
                        res.status(400).send(`El producto ya se encuentra en la compra.`);
                    } else {
                        await venta?.productos.push(producto?.id);
                        await producto?.ventas.push(venta?.id);
                        if (producto) producto.stock = producto.stock - 1;
                        venta.precioTotal += producto.precio;
                        await venta?.save();
                        await producto?.save();
                        res.status(201).json(venta);
                    }
                }


    } catch (error) {
        res.status(500).send('Algo salió mal.');
    }
};

export const borrarProducto = async (req: Request, res: Response) => {
    const ventaId = req?.params?.ventaId;
    const productoId = req?.params?.productoId;

    try {
        let venta = await Venta.findById(ventaId).where({ deleted: false });;
        let producto = await Producto.findById(productoId).where({ deleted: false });;


        if (!venta || !producto)
            res.status(404).send(`No se encontró la venta o el producto indicado.`);
        else
            if (venta.estado == "anulada") {
                res.status(400).send(`La venta se encuentra anulada.`);
            } else {
                // buscar info acerca de transacciones
                let indiceVenta = Number(producto?.ventas.indexOf(ventaId));
                let indiceProducto = Number(venta?.productos.indexOf(productoId));

                if (indiceProducto !== -1 && indiceVenta !== -1) {
                    venta?.productos.splice(indiceProducto, 1);
                    producto?.ventas.splice(indiceVenta, 1);
                    if (producto) producto.stock = producto.stock + 1;
                    venta.precioTotal -= producto.precio;
                    await producto?.save();
                    await venta?.save();
                    res.status(201).json(venta);
                } else {
                    res.status(404).send(`No se encontró la venta o el producto indicado.`);
                }

            }
    } catch (error) {
        res.status(500).send('Algo salió mal.');
    }
};