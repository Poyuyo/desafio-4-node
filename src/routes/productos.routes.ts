import { Router } from 'express';
import { body, param, query } from 'express-validator';
import * as productoController from '../controllers/productos.controller';
import * as ventaController from '../controllers/ventas.controller';
import {validateFields} from '../middlewares/validate-fields'

const router = Router();

router.post('/', [
    body('nombre', 'El nombre es obligatorio').isString(),
    body('precio', 'El precio debe ser mayor que 0').isInt({ min: 1, max: 1000000}),
    body('stock', 'El stock debe ser mayor que 0').isInt({ min: 0}),
    body('estado', 'El estado debe ser nuevo o usado').isString().isIn(['nuevo', 'usado']),
    validateFields
  ], productoController.store);
  
  router.get('/', [
    query('nombre', 'El nombre debe tener como minimo 2 caracteres').optional().isString().isLength({ min: 2, max: 10 }),
    validateFields
  ],productoController.index);
  
  router.get('/:id', [
    param('id', 'El id debe cumplir con el formato de id de mongo').isMongoId(),
    validateFields
  ],productoController.show);
  
  router.put('/:id', [
    param('id', 'El id debe cumplir con el formato de id de mongo').isMongoId(),
    validateFields
  ], productoController.update);

  router.delete('/:id', [
    param('id', 'El id debe cumplir con el formato de id de mongo').isMongoId(),
    validateFields
  ], productoController.destroy);

  //acá uso venta controller ya que la lógica es la misma
  router.post('/:productoId/ventas/:ventaId', 
  [
    param('productoId', 'El productoId debe cumplir con el formato de id de mongo').isMongoId(),
    param('ventaId', 'El ventaId debe cumplir con el formato de id de mongo').isMongoId(),
    validateFields
  ],
  ventaController.agregarProducto);

  router.delete('/:productoId/ventas/:ventaId', [
    param('productoId', 'El productoId debe cumplir con el formato de id de mongo').isMongoId(),
    param('ventaId', 'El ventaId debe cumplir con el formato de id de mongo').isMongoId(),
    validateFields
  ], ventaController.borrarProducto);

export default router;