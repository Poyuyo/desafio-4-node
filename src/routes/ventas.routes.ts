import { Router } from 'express';
import { body } from 'express-validator';
import * as ventaController from '../controllers/ventas.controller';
import { validPersonaEmail } from '../helper/db-validator';
import { validateFields } from '../middlewares/validate-fields';


const router = Router();

router.post('/', ventaController.store);
router.get('/', ventaController.index);
router.get('/:id', ventaController.show);
router.put('/:id', ventaController.update);
router.delete('/:id', ventaController.destroy);
router.post('/:ventaId/productos/:productoId', ventaController.agregarProducto);
router.delete('/:ventaId/productos/:productoId', ventaController.borrarProducto);

export default router;