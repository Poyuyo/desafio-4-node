import { Router } from 'express';
import { body, param } from 'express-validator';
import * as personaController from '../controllers/personas.controller';
import { validEmailLogin, validPersonaEmail } from '../helper/db-validator';
import { validateFields } from '../middlewares/validate-fields';


const router = Router();

router.post('/', [
    body('email').custom(validPersonaEmail),
    validateFields
  ],personaController.store);
router.get('/', personaController.index);

router.get('/:id', [
  param('id', 'El id debe cumplir con el formato de id de mongo').isMongoId(),
  validateFields
], personaController.show);

router.put('/:id', [
  param('id', 'El id debe cumplir con el formato de id de mongo').isMongoId(),
  validateFields
], personaController.update);

router.delete('/:id', [
  param('id', 'El id debe cumplir con el formato de id de mongo').isMongoId(),
  validateFields
], personaController.destroy);

router.post('/login', [
  body('email').custom(validEmailLogin),
  validateFields
], personaController.login)

export default router;