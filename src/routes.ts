import { Application } from 'express';

// Import routes
import ProductosRoutes from './routes/productos.routes';
import PersonasRoutes from './routes/personas.routes';
import VentasRoutes from './routes/ventas.routes';


export const registeredRoutes = async (app:Application) => {
  app.use('/api/productos', ProductosRoutes);
  app.use('/api/personas', PersonasRoutes);
  app.use('/api/ventas', VentasRoutes);
};