import { model, Schema } from 'mongoose';
import IPerson from '../interfaces/personas.interface';

const PersonaSchema = new Schema({
    nombreCompleto: {
        type: String,
        required: [true, 'El nombre es obligatorio']
    },
    email: {
        type: String,
        required: [true, 'El email es obligatorio'],
        unique: true
    },
    contraseña: {
        type: String,
        required: [true, 'La contraseña es obligatoria']
    },
    telefono: {
        type: String,
    },
    rol: {
        type: String,
        required: [true, 'El rol es obligatorio. Valores posibles: admin/cliente'],
        enum: ['admin', 'cliente']
    },
    deleted: {
        type: Boolean,
        default: false
    }
}, {
    timestamps: { createdAt: true, updatedAt: true }
})

export default model<IPerson>('Person', PersonaSchema);