import { model, Schema } from 'mongoose';
import ISale from "../interfaces/ventas.interface";


const VentaSchema = new Schema ({
    formaDePago: {
        type: String,
        required: [true, 'La forma de pago es obligatoria'],
        enum: ['contado', 'tarjeta']
    },
    precioTotal: {
        type: Number
    },
    estado: {
        type: String,
        required: [true, 'El estado es obligatorio'],
        enum: ['aprobada', 'anulada']
    },
    persona_id: {
        type: Schema.Types.ObjectId,
        ref: 'Person',
        required: true
    },
    productos:[{
        type: Schema.Types.ObjectId,
        ref: 'Product'
    }],
    deleted: {
        type: Boolean,
        default: false
    }

},{
    timestamps: { createdAt: true, updatedAt: true }
})

export default model<ISale>('Sale', VentaSchema);